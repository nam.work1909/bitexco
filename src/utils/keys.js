let Keys = {
    keyChecklists: 'checklists',
    keyCallApi: 'callapi',
    keyConnectString: 'connectstring',
    idNew: 'idnew'
};

export default Keys;