import { Screen } from "../utils/device";
export default {
  micro: Screen.width > 320 ? 12 : 10,
  small: 14,
  medium: 18,
  larg: 20,
};
