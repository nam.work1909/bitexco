export const converTypeToColor = (id) => {
    switch (id) {
        case 1: return 'rgb(0, 173, 239)';
        case 2: return 'rgb(239, 111, 0)';
        case 3: return 'rgb(239, 0, 111)';
        case 4: return 'rgb(206, 209, 212)';//'rgb(58, 59, 59)';
        default: break;
    }
}